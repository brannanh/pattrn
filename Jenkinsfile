node {
  stage('Clone repository') {
    checkout scm
  }

  def Set<String> changedProjects

  def String projectsToBuild

  if (env.BRANCH_NAME == 'install-all') {
    stage('Install all jars') {
      withMaven(
        maven: 'maven-3',
        jdk: 'jdk-14',) {
          sh 'mvn install -DskipTests -T 1C'
      }
    }
    currentBuild.result = 'SUCCESS'
    return
  }

  stage('Establish changes') {
    // sh('printenv') - will print all of the environment variables if uncommented, helpful for debugging
    if (env.BRANCH_NAME != 'master') {
      output = sh(script: 'git diff origin/master...$BRANCH_NAME --name-only',
                  returnStdout: true).trim()
    } else {
      changedFiles = [] as Set
      lastSuccessfulBuild = currentBuild.previousSuccessfulBuild
      addChangedPaths(changedFiles, lastSuccessfulBuild.nextBuild)
      output = stringSetToStringArray(changedFiles).join('\n').trim()
    }
    echo output
    changedProjects = getChangedProjects(output)
    echo changedProjects.size().toString()
  }

  stage('Build application') {
    if (changedProjects.size() == 0) {
      projectsToBuild = './frameworks/starter-web'
    } else {
      String[] myArray = stringSetToStringArray(changedProjects)
      projectsToBuild = myArray.join(', ./')
    }
    withMaven(
      maven: 'maven-3',
      jdk: 'jdk-14',) {
        sh 'mvn verify -pl "' + projectsToBuild + '" -DskipTests --amd -T 1C'
    }
  }

  if (env.BRANCH_NAME == 'master') {
    stage('Publish artefacts to nexus') {
      for (project in changedProjects) {
        withMaven(
          maven: 'maven-3',
          jdk: 'jdk-14',) {
            doesTargetExist = sh(script: '[ -d "' + project + '/target" ] && echo "target exists" || echo "target does not exist"', returnStdout: true).trim();
            echo doesTargetExist 
            if (!doesTargetExist.equals('target exists')) {
              sh 'mvn deploy:deploy-file -Dfile=./' + project + '/pom.xml \
                       -DrepositoryId=nexus-snapshots \
                       -Durl=http://nexus:8081/repository/maven-snapshots \
                       -DpomFile=./' + project + '/pom.xml'
            } else {

              output = sh(script: 'ls ./' + project + '/target', returnStdout: true)
              .trim()
              echo output
              filesInTarget = output.split('\\r?\\n')
              jarfile = null
              for (file in filesInTarget) {
                if (file.contains('.jar')) {
                  echo file + ' is a jar!'
                  jarfile = file
                  break
                }
              }

              echo jarfile
              sh 'mvn deploy:deploy-file -Dfile=./' + project + '/target/' + jarfile + ' \
                        -DrepositoryId=nexus-snapshots \
                        -Durl=http://nexus:8081/repository/maven-snapshots \
                        -DpomFile=./' + project + '/pom.xml'
            }
          }
      }
    }

    
    stage('Build and push image') {
      app = docker.build('brannan/pattrn')
      docker.withRegistry('https://registry.hub.docker.com/', 'docker-hub-credentials') {
        app.push('latest')
      }
    }
  }
}

private void addChangedPaths(Set<String> paths, build) {
  for (changeSet in build.changeSets) {
    for (item in changeSet.items) {
      paths.addAll(item.affectedPaths)
    }
  }
  if (build.nextBuild != null) {
    addChangedPaths(paths, build.nextBuild)
  }
}


private String[] stringSetToStringArray(Set<String> inputSet) {
  List<String> myList = new ArrayList<>(inputSet)
      String[] myArray = new String[myList.size()]
      for (int i = 0; i < myList.size(); i++) {
        myArray[i] = myList.get(i)
      }
  return myArray
}

private Set<String> getChangedProjects(String shellScriptOutput) {
  Set<String> poms = [] as Set
  for (String file : shellScriptOutput.split('\\r?\\n')) {
    String toSearchFor = file.replaceAll('/test/', '/main/')
    String locationWithAPom = findNearestPom(toSearchFor)
    if (!locationWithAPom.isEmpty()) {
      poms.add(locationWithAPom)
    }
  }
  return poms
}

private String findNearestPom(String fileName) {
  String fileNameCopy = fileName
  while (fileNameCopy.contains('/')) {
    fileNameCopy = fileNameCopy.substring(0, fileNameCopy.lastIndexOf('/'))
    exists = fileExists(fileNameCopy + '/pom.xml')
    if (exists == true) {
      return fileNameCopy
    }
    fileNameCopy = fileNameCopy.substring(0, fileNameCopy.length() - 1)
  }
  return ''
}
