FROM arm64v8/openjdk:16-buster
RUN adduser --system --group spring
USER spring:spring
ARG JAR_FILE=applications/vanilla/target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]
