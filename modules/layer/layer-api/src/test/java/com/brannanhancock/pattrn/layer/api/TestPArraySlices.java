package com.brannanhancock.pattrn.layer.api;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class TestPArraySlices {

    @Test
    public void testGetSliceWithOneIndexCutOff() {
        // Given
        double[] data = {3D, 2D, 1D, 6D, 5D, 4D, 12D, 11D, 10D, 9D, 8D, 7D};
        int[] dimensions = {2, 2, 3};
        PArray datastore = PArray.createPopulatedArrayOf(data, dimensions);

        // When
        PArray result0 = datastore.getSlice(0);

        // Then
        int[] expectedDimensions = {2, 2};
        assertArrayEquals("Dimensions should be as expected", expectedDimensions, result0.getDimensions());
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 0; j++) {
                assertEquals("Values from slice should be same as from original",
                        datastore.get(i, j, 0), result0.get(i, j), 0D);
            }
        }

        // When
        PArray result1 = datastore.getSlice(1);

        // Then
        assertArrayEquals("Dimensions should be as expected", expectedDimensions, result0.getDimensions());
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 0; j++) {
                assertEquals("Values from slice should be same as from original",
                        datastore.get(i, j, 1), result1.get(i, j), 0D);
            }
        }

        // When
        PArray result2 = datastore.getSlice(2);

        // Then
        assertArrayEquals("Dimensions should be as expected", expectedDimensions, result0.getDimensions());
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 0; j++) {
                assertEquals("Values from slice should be same as from original",
                        datastore.get(i, j, 2), result2.get(i, j), 0D);
            }
        }
    }


    @Test
    public void testGetSliceWithTwoIndicesCutOff() {
        // Given
        double[] data = {3D, 2D, 1D, 6D, 5D, 4D, 12D, 11D, 10D, 9D, 8D, 7D};
        int[] dimensions = {2, 2, 3};
        PArray datastore = PArray.createPopulatedArrayOf(data, dimensions);

        // When
        PArray result00 = datastore.getSlice(0, 0);

        // Then
        int[] expectedDimensions = {2};
        assertArrayEquals("Dimensions should be as expected", expectedDimensions, result00.getDimensions());
        for (int i = 0; i < 2; i++) {
            assertEquals("Values from slice should be same as from original",
                        datastore.get(i, 0, 0), result00.get(i), 0D);

        }

        // When
        PArray result10 = datastore.getSlice(1, 0);

        // Then
        assertArrayEquals("Dimensions should be as expected", expectedDimensions, result10.getDimensions());
        for (int i = 0; i < 2; i++) {
            assertEquals("Values from slice should be same as from original",
                    datastore.get(i, 1, 0), result10.get(i), 0D);

        }

        // When
        PArray result01 = datastore.getSlice(0, 1);

        // Then
        assertArrayEquals("Dimensions should be as expected", expectedDimensions, result01.getDimensions());
        for (int i = 0; i < 2; i++) {
            assertEquals("Values from slice should be same as from original",
                    datastore.get(i, 0, 1), result01.get(i), 0D);

        }

        // When
        PArray result11 = datastore.getSlice(1, 1);

        // Then
        assertArrayEquals("Dimensions should be as expected", expectedDimensions, result11.getDimensions());
        for (int i = 0; i < 2; i++) {
            assertEquals("Values from slice should be same as from original",
                    datastore.get(i, 1, 1), result11.get(i), 0D);

        }
    }


    @Test(expected = IllegalArgumentException.class)
    public void testTooManyCoordinates() {
        // Given
        PArray array = PArray.createEmptyPArrayWithDimensions(2, 3);

        // When
        array.getSlice(1, 2, 3);

        // Then - expect exception
    }


    @Test(expected = IllegalArgumentException.class)
    public void testOutOfBoundsCoordinates() {
        // Given
        PArray array = PArray.createEmptyPArrayWithDimensions(2, 3);

        // When
        array.getSlice(3);

        // Then - expect exception
    }


    @Test(expected = IllegalArgumentException.class)
    public void testTooManyCoordinatesSettingSlice() {
        // Given
        PArray array = PArray.createEmptyPArrayWithDimensions(2, 3);
        PArray toInsert = PArray.createEmptyPArrayWithDimensions(2);

        // When
        array.setSlice(toInsert, 1, 2, 3);

        // Then - expect exception
    }


    @Test(expected = IllegalArgumentException.class)
    public void testOutOfBoundsCoordinatesSettingSlice() {
        // Given
        PArray array = PArray.createEmptyPArrayWithDimensions(2, 3);
        PArray toInsert = PArray.createEmptyPArrayWithDimensions(2);

        // When
        array.setSlice(toInsert,3);

        // Then - expect exception
    }


    @Test(expected = IllegalArgumentException.class)
    public void testInsertTooManyDimensions() {
        // Given
        PArray array = PArray.createEmptyPArrayWithDimensions(2, 3);
        PArray toInsert = PArray.createEmptyPArrayWithDimensions(3, 4);

        // When
        array.setSlice(toInsert,1);

        // Then - expect exception
    }


    @Test(expected = IllegalArgumentException.class)
    public void testInsertTooFewDimensions() {
        // Given
        PArray array = PArray.createEmptyPArrayWithDimensions(2, 3, 4);
        PArray toInsert = PArray.createEmptyPArrayWithDimensions(3);

        // When
        array.setSlice(toInsert,1);

        // Then - expect exception
    }


    @Test(expected = IllegalArgumentException.class)
    public void testMisfitSlice() {
        // Given
        PArray array = PArray.createEmptyPArrayWithDimensions(2, 3, 4);
        PArray toInsert = PArray.createEmptyPArrayWithDimensions(1, 3);

        // When
        array.setSlice(toInsert,1);

        // Then - expect exception
    }


    @Test
    public void testSetSlices() {
        // Given
        PArray datastore = PArray.createEmptyPArrayWithDimensions(2, 2, 3);
        int[] sliceDimensions = {2, 2};
        double[] values0 = {4D, 3D, 1D, 2D};
        PArray toInsert0 = PArray.createPopulatedArrayOf(values0, sliceDimensions);
        double[] values1 = {7D, 6D, 8D, 5D};
        PArray toInsert1 = PArray.createPopulatedArrayOf(values1, sliceDimensions);
        double[] values2 = {48D, 12.9, 97.2, 24624.8};
        PArray toInsert2 = PArray.createPopulatedArrayOf(values2, sliceDimensions);

        // When
        datastore.setSlice(toInsert0, 0);
        datastore.setSlice(toInsert1, 1);
        datastore.setSlice(toInsert2, 2);

        // Then
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                assertEquals("Values should be as expected", toInsert0.get(i, j), datastore.get(i, j, 0), 0D);
                assertEquals("Values should be as expected", toInsert1.get(i, j), datastore.get(i, j, 1), 0D);
                assertEquals("Values should be as expected", toInsert2.get(i, j), datastore.get(i, j, 2), 0D);
            }
        }

        PArray retrieved0 = datastore.getSlice(0);
        PArray retrieved1 = datastore.getSlice(1);
        PArray retrieved2 = datastore.getSlice(2);
        assertEquals("Should be equality between the set and retrieved slices", toInsert0, retrieved0);
        assertEquals("Should be equality between the set and retrieved slices", toInsert1, retrieved1);
        assertEquals("Should be equality between the set and retrieved slices", toInsert2, retrieved2);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testComposeMismatchedSlices() {
        // Given
        int[] sliceDimensions0 = {2, 2};
        int[] sliceDimensions1 = {2, 3};
        double[] values0 = {4D, 3D, 1D, 2D};
        PArray toInsert0 = PArray.createPopulatedArrayOf(values0, sliceDimensions0);
        double[] values1 = {7D, 6D, 8D, 5D, 10D, 9D};
        PArray toInsert1 = PArray.createPopulatedArrayOf(values1, sliceDimensions1);

        // When
        PArray.composeFromSlices(Arrays.asList(toInsert0, toInsert1));

        // Then - expect exeption
    }


    @Test
    public void testComposeSlices() {
        // Given
        int[] sliceDimensions = {2, 2};
        double[] values0 = {4D, 3D, 1D, 2D};
        PArray toInsert0 = PArray.createPopulatedArrayOf(values0, sliceDimensions);
        double[] values1 = {7D, 6D, 8D, 5D};
        PArray toInsert1 = PArray.createPopulatedArrayOf(values1, sliceDimensions);
        double[] values2 = {48D, 12.9, 97.2, 24624.8};
        PArray toInsert2 = PArray.createPopulatedArrayOf(values2, sliceDimensions);

        // When
        PArray datastore = PArray.composeFromSlices(Arrays.asList(toInsert0, toInsert1, toInsert2));

        // Then
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                assertEquals("Values should be as expected", toInsert0.get(i, j), datastore.get(i, j, 0), 0D);
                assertEquals("Values should be as expected", toInsert1.get(i, j), datastore.get(i, j, 1), 0D);
                assertEquals("Values should be as expected", toInsert2.get(i, j), datastore.get(i, j, 2), 0D);
            }
        }

        PArray retrieved0 = datastore.getSlice(0);
        PArray retrieved1 = datastore.getSlice(1);
        PArray retrieved2 = datastore.getSlice(2);
        assertEquals("Should be equality between the set and retrieved slices", toInsert0, retrieved0);
        assertEquals("Should be equality between the set and retrieved slices", toInsert1, retrieved1);
        assertEquals("Should be equality between the set and retrieved slices", toInsert2, retrieved2);
    }
}
