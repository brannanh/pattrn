package com.brannanhancock.pattrn.layer.api;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMultiplication {

    @Test(expected = IllegalArgumentException.class)
    public void testInnerProductMisfit() {
        // Given
        PArray parray1 = PArray.createEmptyPArrayWithDimensions(2,3,4);
        PArray parray2 = PArray.createEmptyPArrayWithDimensions(2,3);

        // When
        parray1.innerProduct(parray2);

        // Then - expect exception
    }


    @Test
    public void testInnerProduct() {
        // Given
        double[] input0 = {2D, 3D, 4D, 1D};
        double[] input1 = {1D, 2D, 3D, 4D};
        int[] inputDimensions = {2, 2};

        PArray parray1 = PArray.createPopulatedArrayOf(input0, inputDimensions);
        PArray parray2 = PArray.createPopulatedArrayOf(input1, inputDimensions);

        // When
        PArray product = parray1.innerProduct(parray2);

        // Then
        assertEquals("Value should be as expected", 2D, product.get(0,0), 0D);
        assertEquals("Value should be as expected", 6D, product.get(1,0), 0D);
        assertEquals("Value should be as expected", 12D, product.get(0,1), 0D);
        assertEquals("Value should be as expected", 4D, product.get(1,1), 0D);
    }

    @Test
    public void testScalarProduct() {
        // Given
        double[] input0 = {2D, 3D, 4D, 1D};
        int[] inputDimensions = {2, 2};

        PArray parray = PArray.createPopulatedArrayOf(input0, inputDimensions);

        // When
        PArray product = parray.scalarProduct(7D);

        // Then
        assertEquals("Value should be as expected", 14D, product.get(0,0), 0D);
        assertEquals("Value should be as expected", 21D, product.get(1,0), 0D);
        assertEquals("Value should be as expected", 28D, product.get(0,1), 0D);
        assertEquals("Value should be as expected", 7D, product.get(1,1), 0D);
    }
}
