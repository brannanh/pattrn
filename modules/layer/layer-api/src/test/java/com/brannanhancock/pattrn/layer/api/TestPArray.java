package com.brannanhancock.pattrn.layer.api;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestPArray {

    @Test
    public void testFullSetAndRetrive() {
        // Given
        int[] dimensions = {5, 6, 7};
        PArray array = PArray.createEmptyPArrayWithDimensions(dimensions);

        double value = 0;
        for (int coord1 = 0; coord1 < 5; coord1++) {
            for (int coord2 = 0; coord2 < 6; coord2++) {
                for (int coord3 = 0; coord3 < 7; coord3++) {
                    array.set(value, coord1, coord2, coord3);
                    value++;
                }
            }
        }

        // Then
        value = 0;
        for (int coord1 = 0; coord1 < 5; coord1++) {
            for (int coord2 = 0; coord2 < 6; coord2++) {
                for (int coord3 = 0; coord3 < 7; coord3++) {
                    assertEquals("Value should be as when enetered", value, array.get(coord1, coord2, coord3), 0);
                    value++;
                }
            }
        }
    }

    @Test
    public void testFullRetrieveFromInitialise() {
        // Given
        double[][][] values =
                {
                    {
                        {
                            12D, 2D, 3D
                        },
                        {
                            4D, 8D, 6D
                        }
                    },
                    {
                        {
                            7D, 5D, 9D,
                        },
                        {
                            10D, 11D, 1D
                        }
                    }
                };
        PArray array = PArray.createThreeDimensionalArrayOf(values);

        // Then
        assertEquals("Value should be as expected", 12D, array.get(0,0,0), 0);
        assertEquals("Value should be as expected", 2D, array.get(0,0,1), 0);
        assertEquals("Value should be as expected", 3D, array.get(0,0,2), 0);
        assertEquals("Value should be as expected", 4D, array.get(0,1,0), 0);
        assertEquals("Value should be as expected", 8D, array.get(0,1,1), 0);
        assertEquals("Value should be as expected", 6D, array.get(0,1,2), 0);
        assertEquals("Value should be as expected", 7D, array.get(1,0,0), 0);
        assertEquals("Value should be as expected", 5D, array.get(1,0,1), 0);
        assertEquals("Value should be as expected", 9D, array.get(1,0,2), 0);
        assertEquals("Value should be as expected", 10D, array.get(1,1,0), 0);
        assertEquals("Value should be as expected", 11D, array.get(1,1,1), 0);
        assertEquals("Value should be as expected", 1D, array.get(1,1,2), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalDataAndDimensions() {
        // Given
        int[] dimensions = {3,4};
        double[] data = new double[10];

        // When
        PArray.createPopulatedArrayOf(data, dimensions);

        // Then - expect exception
    }


    @Test
    public void testPreFormattedOneDimension() {
        // Given
        double[] data = {5, 6, 7, 8};
        PArray array = PArray.createOneDimensionalArrayOf(data);

        // Then
        assertEquals("Value should be as expected", 5D, array.get(0));
        assertEquals("Value should be as expected", 6D, array.get(1));
        assertEquals("Value should be as expected", 7D, array.get(2));
        assertEquals("Value should be as expected", 8D, array.get(3));
    }


    @Test
    public void testPreFormattedTwoDimension() {
        // Given
        double[][] data = {{7, 6}, {8, 5}};
        PArray array = PArray.createTwoDimensionalArrayOf(data);

        // Then
        assertEquals("Value should be as expected", 7D, array.get(0, 0));
        assertEquals("Value should be as expected", 6D, array.get(0, 1));
        assertEquals("Value should be as expected", 8D, array.get(1, 0));
        assertEquals("Value should be as expected", 5D, array.get(1, 1));
    }


    @Test
    public void testPreFormattedThreeDimension() {
        // Given
        double[][][] data = {{{7, 6}, {8, 5}}, {{4, 1}, {2, 3}}};
        PArray array = PArray.createThreeDimensionalArrayOf(data);

        // Then
        assertEquals("Value should be as expected", 7D, array.get(0, 0, 0));
        assertEquals("Value should be as expected", 6D, array.get(0, 0, 1));
        assertEquals("Value should be as expected", 8D, array.get(0, 1, 0));
        assertEquals("Value should be as expected", 5D, array.get(0, 1, 1));
        assertEquals("Value should be as expected", 4D, array.get(1, 0, 0));
        assertEquals("Value should be as expected", 1D, array.get(1, 0, 1));
        assertEquals("Value should be as expected", 2D, array.get(1, 1, 0));
        assertEquals("Value should be as expected", 3D, array.get(1, 1, 1));
    }


    @Test(expected = IllegalArgumentException.class)
    public void testRetrieveRejectsNegative() {
        // Given
        int[] dimensions = {32, 32, 3, 5};
        PArray array = PArray.createEmptyPArrayWithDimensions(dimensions);

        // When
        array.get(0, 0, -1, 0);

        // Then - expect exception
    }


    @Test(expected = IllegalArgumentException.class)
    public void testRetrieveWithTooFewCoordinates() {
        // Given
        int[] dimensions = {32, 32, 3, 5};
        PArray array = PArray.createEmptyPArrayWithDimensions(dimensions);

        // When
        array.get(0, 0, 0);

        // Then - expect exception
    }


    @Test(expected = IllegalArgumentException.class)
    public void testRetrieveWithTooManyCoordinates() {
        // Given
        int[] dimensions = {32, 32};
        PArray array = PArray.createEmptyPArrayWithDimensions(dimensions);

        // When
        array.get(0, 0, 0);

        // Then - expect exception
    }


    @Test(expected = IllegalArgumentException.class)
    public void testRetriveOutOfBounds() {
        // Given
        int[] dimensions = {32, 32};
        PArray array = PArray.createEmptyPArrayWithDimensions(dimensions);

        // When
        array.get(0, 32);

        // Then - expect exception
    }
}
