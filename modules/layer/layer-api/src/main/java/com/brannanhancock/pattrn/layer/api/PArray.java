package com.brannanhancock.pattrn.layer.api;

import java.util.Arrays;
import java.util.Collection;

public class PArray {

    private final double[] data;
    private final int[] dimensions;
    private final int[] initialPositions;


    /**
     * Create an empty PArray with the defined dimensions
     * @param dimensions - the dimensionality of this multidimensional array
     */
    public static PArray createEmptyPArrayWithDimensions(int... dimensions) {
        return new PArray(dimensions);
    }


    /**
     * Create a populated PArray with the specified data casted to the specified dimensions.
     * @param values - the values to be cast into the specified dimensions
     * @param dimensions - the dimensionality of this multidimensional array
     */
    public static PArray createPopulatedArrayOf(double[] values, int... dimensions) {
        return new PArray(values.clone(), dimensions.clone());
    }


    /**
     * Create a populated one dimensional PArray with the given values.
     * @param data - The data for this one dimensional PArray.
     */
    public static PArray createOneDimensionalArrayOf(double[] data) {
        return new PArray(data);
    }


    /**
     * Create a populated two dimensional PArray with the given values
     * @param data - The data for this two dimensional PArray.
     */
    public static PArray createTwoDimensionalArrayOf(double[][] data) {
        return new PArray(data);
    }


    /**
     * Create a populated three dimensional PArray with the given values
     * @param data - the data for this three dimensional PArray.
     */
    public static PArray createThreeDimensionalArrayOf(double[][][] data) {
        return new PArray(data);
    }


    public void set(double value, int... coordinates) {
        validate(coordinates);
        data[locationFor(coordinates)] = value;
    }


    public double get(int... coordinates) {
        validate(coordinates);
        return data[locationFor(coordinates)];
    }


    public static PArray composeFromSlices(PArray... slices) {
        for (int i = 1; i < slices.length; i++) {
            if (!Arrays.equals(slices[0].dimensions, slices[i].dimensions)) {
                throw new IllegalArgumentException("A PArray can only be composed from slices with the same " +
                        "dimensions. [0th Slice Dimensions: " + Arrays.toString(slices[0].dimensions) + ", " + i +
                        "th Slice Dimensions: " + Arrays.toString(slices[i].dimensions) + "]");
            }
        }

        int[] composedDimensions = new int[slices[0].dimensions.length + 1];
        for (int i = 0; i < slices[0].dimensions.length; i++) {
            composedDimensions[i] = slices[0].dimensions[i];
        }
        composedDimensions[composedDimensions.length-1] = slices.length;
        PArray toReturn = PArray.createEmptyPArrayWithDimensions(composedDimensions);
        for (int i = 0; i < slices.length; i++) {
            toReturn.setSlice(slices[i], i);
        }
        return toReturn;
    }


    public static PArray composeFromSlices(Collection<PArray> slices) {
        PArray[] toCompose = new PArray[slices.size()];
        return composeFromSlices(slices.toArray(toCompose));
    }


    /**
     * Allows you to take a slice of the PArray by declaring the last coodrinates of the slice. For example if you
     * have a PArray which is 5 x 6 x 7 x 8 in its dimensionality and you pass in 2, 3 you will receive back a PArray
     * of dimensions 5 x 6 x 7, with the values in place of [*, *, *, 2, 3] of the original PArray.
     *
     * The original PArray is unaffeceted by this operation.
     * @param sliceCoordinates
     * @return
     */
    public PArray getSlice(int... sliceCoordinates) {
        validateSliceCoordinates(sliceCoordinates);
        int[] fromCoords = replaceLastValuesWith(new int[dimensions.length], sliceCoordinates);
        int[] maxCoordinates = dimensions.clone();
        for (int i = 0; i < maxCoordinates.length; i++) {
            maxCoordinates[i]--;
        }
        int[] toCoords = replaceLastValuesWith(maxCoordinates, sliceCoordinates);
        int from = locationFor(fromCoords);
        int to = locationFor(toCoords);
        double[] slicedData = Arrays.copyOfRange(data, from, to + 1);
        int[] sliceDimensions = new int[dimensions.length - sliceCoordinates.length];
        for (int i = 0; i < sliceDimensions.length; i++) {
            sliceDimensions[i] = dimensions[i];
        }
        return PArray.createPopulatedArrayOf(slicedData, sliceDimensions);
    }


    /**
     * Allows you to set a slice of the PArray by declaring the last coordinates of the slice. For example if you have
     * a PArray which is 5 x 6 x 7 x 8 x 9 in its dimensionality and you pass in a 5 x 6 x 7 PArray with
     * sliceCoodrintes 2, 3, the values in the PArray occupied by [*, *, *, 2, 3] will be set equal to the input slice.
     * @param toInsert
     * @param sliceCoordinates
     * @return
     */
    public PArray setSlice(PArray toInsert, int... sliceCoordinates) {
        validateSliceCoordinates(sliceCoordinates);
        if (toInsert.dimensions.length + sliceCoordinates.length != dimensions.length) {
            throw new IllegalArgumentException("A slice must fit perfectly within the coordinates specified" +
                    "[sliceDimensions: " + Arrays.toString(toInsert.dimensions) + ", sliceCoordinates: "
                    + Arrays.toString(sliceCoordinates) + ", array dimensions: " + Arrays.toString(dimensions) + "]");
        }
        for (int i = 0; i < toInsert.dimensions.length; i++) {
            if (toInsert.dimensions[i] != dimensions[i]) {
                throw new IllegalArgumentException("A slice must fit perfectly within the coordinates specified" +
                        "[sliceDimensions: " + Arrays.toString(toInsert.dimensions) + ", array dimensions: "
                        + Arrays.toString(dimensions) + "]");
            }
        }
        int[] fromCoords = replaceLastValuesWith(new int[dimensions.length], sliceCoordinates);
        int from = locationFor(fromCoords);
        int length = Arrays.stream(toInsert.dimensions).reduce(1, (a, b) -> a*b);
        System.arraycopy(toInsert.data, 0, data, from, length);
        return this;
    }

    /**
     * Calculate the product of two PArrays. Neither of the input arrays are mutated, a new PArray instance is created
     * with the data in place.
     * @param other
     * @return - a new instance where each element is the product of the two elements in that same position in the
     * input arrays.
     */
    public PArray innerProduct(PArray other) {
        if (!Arrays.equals(dimensions, other.dimensions)) {
            throw new IllegalArgumentException("PArrays must have the same dimensions to calculate their inner" +
                    "product. [dimensions: " + Arrays.toString(dimensions) + ", otherDimensions: " +
                    Arrays.toString(other.dimensions) + "]");
        }
        double[] valuesToReturn = new double[data.length];
        for (int i = 0; i < valuesToReturn.length; i++) {
            valuesToReturn[i] = data[i] * other.data[i];
        }
        return PArray.createPopulatedArrayOf(valuesToReturn, dimensions.clone());
    }


    /**
     * Calculate the product of a PArray and a scalar. The PArray's data is not mutated, a new PArray instance is
     * created with the results in place.
     * @param scalar
     * @return
     */
    public PArray scalarProduct(double scalar) {
        PArray toReturn = PArray.createPopulatedArrayOf(data, dimensions);
        for (int i = 0; i < toReturn.data.length; i++) {
            toReturn.data[i]*=scalar;
        }
        return toReturn;
    }


    private void validateSliceCoordinates(int... sliceCoordinates) {
        if (sliceCoordinates.length >= dimensions.length) {
            throw new IllegalArgumentException("Slice coordinates must be shorter than the number of dimensions of the" +
                    "the PArray. [Dimension length: " + dimensions.length + ", sliceCoordinates length: " +
                    sliceCoordinates.length + "]");
        }
        for (int i = 0; i < sliceCoordinates.length; i ++) {
            if (sliceCoordinates[i] >= dimensions[dimensions.length - (i + 1)]) {
                throw new IllegalArgumentException("Slice coordinates must be within the bounds of the dimensions of" +
                        "this PArray. [PArray dimensions: " + dimensions + ", sliceCoordinates: " + sliceCoordinates +
                        "]");
            }
        }
    }


    private int[] replaceLastValuesWith(int[] original, int[] replacement) {
        int[] clone = original.clone();
        int count = 0;
        for (int i = original.length - replacement.length; i< original.length; i++) {
            clone[i] = replacement[count];
            count++;
        }
        return clone;
    }

    public int[] getDimensions() {
        return dimensions.clone();
    }


    private PArray(int... dimensions) {
        this.dimensions = dimensions;
        this.data = new double[Arrays.stream(dimensions).reduce(1, (a, b) -> a*b)];
        this.initialPositions = createInitialPositionsFor(dimensions);
    }


    private PArray(double[] values, int... dimensions) {
        if (values.length != Arrays.stream(dimensions).reduce(1, (a, b) -> a*b)) {
            throw new IllegalArgumentException("Cannot cast data to specified dimensions: [data length: "
                    + values.length + ", dimensions : " + Arrays.toString(dimensions) + "]");
        }
        this.dimensions = dimensions;
        this.initialPositions = createInitialPositionsFor(dimensions);
        this.data = values;
    }


    private PArray(double[] data) {
        this.dimensions = new int[1];
        dimensions[0] = data.length;
        this.initialPositions = createInitialPositionsFor(dimensions);
        this.data = new double[dimensions[0]];
        for (int i = 0; i < dimensions[0]; i++) {
            this.data[i] = data[i];
        }
    }


    private PArray(double[][] values) {
        this.dimensions = new int[2];
        dimensions[0] = values.length;
        dimensions[1] = values[0].length;
        this.initialPositions = createInitialPositionsFor(dimensions);
        this.data = new double[dimensions[0]*dimensions[1]];
        for (int i = 0; i < dimensions[0]; i++) {
            for (int j = 0; j <dimensions[1]; j++) {
                data[locationFor(i, j)] = values[i][j];
            }
        }
    }


    private PArray(double[][][] values) {
        this.dimensions = new int[3];
        dimensions[0] = values.length;
        dimensions[1] = values[0].length;
        dimensions[2] = values[0][0].length;
        this.initialPositions = createInitialPositionsFor(dimensions);
        this.data = new double[dimensions[0]*dimensions[1]*dimensions[2]];
        for (int i = 0; i < dimensions[0]; i++) {
            for (int j = 0; j <dimensions[1]; j++) {
                for (int k = 0; k < dimensions[2]; k++) {
                    data[locationFor(i, j, k)] = values[i][j][k];
                }
            }
        }
    }


    private static int[] createInitialPositionsFor(int[] dimensions) {
        int[] initialPositions = new int[dimensions.length];
        int value = 1;
        initialPositions[0] = 1;
        for (int i = 1; i < dimensions.length; i++) {
            value *= dimensions[i-1];
            initialPositions[i] = value;
        }
        return initialPositions;
    }

    private int locationFor(int... coordinates) {
        int position = 0;
        for (int i = 0; i < coordinates.length; i++) {
            position += coordinates[i] * initialPositions[i];
        }
        return position;
    }

    private void validate(int[] coordinates) {
        if (coordinates.length != dimensions.length) {
            throw new IllegalArgumentException("Must specify the same number of co-ordinates as there are dimensions." +
                    " [Supplied: " + coordinates.length + ", required: " + dimensions.length + "]");
        }
        for (int i = 0; i < coordinates.length; i++) {
            if (coordinates[i] >= dimensions[i]) {
                throw new IllegalArgumentException("Coordinate out of bounds, [dimensions: " + dimensions +
                        ", coordinates: " + coordinates + "]");
            }
        }
        Arrays.stream(coordinates)
                .filter(input -> input < 0)
                .findAny()
                .ifPresent(input -> {throw new IllegalArgumentException("Negative co-ordinates are not allowed");});
    }


    @Override
    public String toString() {
        return "PArray{" +
                "dimensions=" + Arrays.toString(dimensions) +
                ", initalPositions=" + Arrays.toString(initialPositions) +
                ", data=" + Arrays.toString(data) +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PArray pArray = (PArray) o;
        return Arrays.equals(data, pArray.data) &&
                Arrays.equals(dimensions, pArray.dimensions) &&
                Arrays.equals(initialPositions, pArray.initialPositions);
    }


    @Override
    public int hashCode() {
        int result = Arrays.hashCode(data);
        result = 31 * result + Arrays.hashCode(dimensions);
        result = 31 * result + Arrays.hashCode(initialPositions);
        return result;
    }
}
