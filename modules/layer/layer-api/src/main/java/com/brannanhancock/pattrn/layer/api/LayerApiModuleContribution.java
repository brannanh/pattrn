package com.brannanhancock.pattrn.layer.api;

import com.brannanhancock.pattrn.coreconcept.api.ModuleContribution;
import org.springframework.stereotype.Component;

@Component
public class LayerApiModuleContribution implements ModuleContribution {
    @Override
    public String getModuleName() {
        return "Layer API";
    }
}
