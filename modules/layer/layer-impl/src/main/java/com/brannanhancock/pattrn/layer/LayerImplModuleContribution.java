package com.brannanhancock.pattrn.layer;

import com.brannanhancock.pattrn.coreconcept.api.ModuleContribution;
import org.springframework.stereotype.Component;

@Component
public class LayerImplModuleContribution implements ModuleContribution {
    @Override
    public String getModuleName() {
        return "Layer Impl";
    }
}
