package com.brannanhancock.pattrn.user.api;

import com.brannanhancock.pattrn.coreconcept.api.ModuleContribution;
import org.springframework.stereotype.Component;

@Component
public class UserApiModuleContribution implements ModuleContribution {
    @Override
    public String getModuleName() {
        return "User API";
    }
}
