package com.brannanhancock.pattrn.user;

import com.brannanhancock.pattrn.coreconcept.api.ModuleContribution;
import org.springframework.stereotype.Component;

@Component
public class UserImplModuleContribution implements ModuleContribution {
    @Override
    public String getModuleName() {
        return "User Impl3";
    }
}
