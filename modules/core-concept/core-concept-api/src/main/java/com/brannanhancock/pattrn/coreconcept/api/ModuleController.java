package com.brannanhancock.pattrn.coreconcept.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public interface ModuleController {

    @GetMapping(path = "/modules")
    public List<String> modules();
}
