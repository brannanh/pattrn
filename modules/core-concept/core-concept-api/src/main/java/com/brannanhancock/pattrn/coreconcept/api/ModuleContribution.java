package com.brannanhancock.pattrn.coreconcept.api;

public interface ModuleContribution {

    String getModuleName();
}
