package com.brannanhancock.pattrn.coreconcept.api;

import org.springframework.stereotype.Component;

@Component
public class CoreConceptApiModuleContribution implements ModuleContribution {

    @Override
    public String getModuleName() {
        return "Core Concepts API3";
    }
}
