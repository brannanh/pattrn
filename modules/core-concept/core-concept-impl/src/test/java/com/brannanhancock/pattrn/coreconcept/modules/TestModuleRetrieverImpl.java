package com.brannanhancock.pattrn.coreconcept.modules;

import com.brannanhancock.pattrn.coreconcept.api.ModuleContribution;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestModuleRetrieverImpl {

    @Test
    public void testDelegation() {
        // Given
        ModuleContribution contribution1 = mock(ModuleContribution.class);
        ModuleContribution contribution2 = mock(ModuleContribution.class);
        ModuleContribution contribution3 = mock(ModuleContribution.class);
        when(contribution1.getModuleName()).thenReturn("Module1");
        when(contribution2.getModuleName()).thenReturn("Module2");
        when(contribution3.getModuleName()).thenReturn("Module3");
        ModuleRetriever sut = new ModuleRetriever(List.of(contribution1, contribution2, contribution3));

        // When
        Collection<String> result = sut.getModules();

        // Then
        assertTrue("Contribution1 should have been delegated to", result.contains("Module1"));
        assertTrue("Contribution2 should have been delegated to", result.contains("Module2"));
        assertTrue("Contribution3 should have been delegated to", result.contains("Module3"));
    }

    @Test
    public void testSafetyAdd() {
        // Given
        ModuleContribution contribution1 = mock(ModuleContribution.class);
        when(contribution1.getModuleName()).thenReturn("Module1");
        ModuleRetriever sut = new ModuleRetriever(List.of(contribution1));

        // When
        Collection<String> result = sut.getModules();

        // Then
        assertTrue("Contribution1 should have been delegated to", result.contains("Module1"));
        try {
            result.add("Random String");
        } catch (RuntimeException e) {
            return;
        }
        Assert.fail("Trying to add to the returned list should have thrown a runtime exception");
    }

    @Test
    public void testSafetyRemove() {
        // Given
        ModuleContribution contribution1 = mock(ModuleContribution.class);
        when(contribution1.getModuleName()).thenReturn("Module1");
        ModuleRetriever sut = new ModuleRetriever(List.of(contribution1));

        // When
        Collection<String> result = sut.getModules();

        // Then
        assertTrue("Contribution1 should have been delegated to", result.contains("Module1"));
        try {
            result.removeIf(s -> true);
        } catch (RuntimeException e) {
            return;
        }
        Assert.fail("Trying to remove elements should have thrown a runtime exception");
    }
}
