package com.brannanhancock.pattrn.coreconcept.modules;

import com.brannanhancock.pattrn.coreconcept.api.ModuleContribution;
import org.springframework.stereotype.Component;

@Component
public class CoreConceptImplModuleContribution implements ModuleContribution {

    @Override
    public String getModuleName() {
        return "Core Concepts Impl2";
    }
}
