package com.brannanhancock.pattrn.coreconcept.modules;

import com.brannanhancock.pattrn.coreconcept.api.ModuleContribution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
class ModuleRetriever {

    private final List<ModuleContribution> moduleContributionList;

    @Autowired
    ModuleRetriever(List<ModuleContribution> moduleContributionList) {
        this.moduleContributionList = moduleContributionList;
    }

    public Collection<String> getModules() {
        return moduleContributionList.stream()
                .map(ModuleContribution::getModuleName)
                .collect(Collectors.toUnmodifiableList());
    }
}
