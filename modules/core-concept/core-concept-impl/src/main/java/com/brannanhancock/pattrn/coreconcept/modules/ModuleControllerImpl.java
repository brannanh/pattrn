package com.brannanhancock.pattrn.coreconcept.modules;

import com.brannanhancock.pattrn.coreconcept.api.ModuleController;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class ModuleControllerImpl implements ModuleController {

    private final ModuleRetriever retriever;

    ModuleControllerImpl(ModuleRetriever retriever) {
        this.retriever = retriever;
    }

    @Override
    public List<String> modules() {
        return Collections.unmodifiableList(new ArrayList<>(retriever.getModules()));
    }
}
