package com.brannanhancock.pattrn.vanilla;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.brannanhancock.pattrn.vanilla",
		"com.brannanhancock.pattrn.coreconcept",
		"com.brannanhancock.pattrn.layer",
		"com.brannanhancock.pattrn.user",
        "com.brannanhancock.pattrn.starterweb"})
public class CnnApplication {

	public static void main(String[] args) {
		SpringApplication.run(CnnApplication.class, args);
	}
}
